Projekt:	a4, TI1-P1 SS14
Organisation:	HAW-Hamburg, Informatik
Team:		S1T5
Autor(en):
Baha, Mir Farshid	(mirfarshid.baha@haw-hamburg.de)
G�tze, Nico		(nico.goetze@haw-hamburg.de)

Review History
===========================
140408 v1.0 failed Schafers algorithmic improvements necessary
140410 v1.1 improved the algorithm, got rid of tabs
140422 v2.0 failed Schafers wants 2 visible Iterations 