package a4;

/**
 * TeamNr:S1T5 <Mir Farshid, Baha> (<2141801>,<mirfarshid.baha@haw-hamburg.de>),
 * <Nico, G�tze> (<2125118>,<nico.goetze@haw-hamburg.de>),
 */
public class PrimeFactorization {
    public static void main(String[] args) {
        final long input = 134671454456l;

        long nochZuZerlegendeZahl = input;
        if (nochZuZerlegendeZahl <= 1)
            System.out.println("invalid input");
        else {
            long wurzel = (long) Math.sqrt(nochZuZerlegendeZahl);
            System.out.print(nochZuZerlegendeZahl + " = ");
            int primFaktorKandidat = 2;
            while (nochZuZerlegendeZahl > 1) {
                while (nochZuZerlegendeZahl % primFaktorKandidat == 0) {
                    nochZuZerlegendeZahl = nochZuZerlegendeZahl / primFaktorKandidat;
                    System.out.print(primFaktorKandidat);
                    if (nochZuZerlegendeZahl > 1)
                        System.out.print("*");
                }
                primFaktorKandidat++;
                if(primFaktorKandidat > wurzel){
                    System.out.println(nochZuZerlegendeZahl);
                    nochZuZerlegendeZahl=0;
                }
            }
        }
    }
}
